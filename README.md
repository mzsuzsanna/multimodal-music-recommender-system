# Computational Cognitive Science 2 (2024)

This is the code of my project called 'Music to my Ears: Multimodal Song Recommender System for Implicit Feedback' on a smaller version of the KGRec Music Recommendation Dataset.

## Prerequisites
Environment: Jupyter Notebook (python 3.11v)
Except for the openl3 library, which is comptabile with python 3.6, 3.7, 3.8

Relevant libraries used in this project:\
matplotlib==3.8.0\
pandas==2.2.1\
seaborn==0.13.2\

nltk==3.8.1\
numpy==1.26.4\
scikit-learn==1.3.0\
sklearn==0.0.post9\
spacy==3.7.2\
tokenizers==0.15.2\
tqdm==4.66.1\
gensim==4.3.2\
librosa==0.10.2.post1\
openl3==0.4.2\
sentence-transformers==2.7.0
tensorflow==2.16.1\
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.7.1/en_core_web_sm-3.7.1-py3-none-any.whl#sha256=86cc141f63942d4b2c5fcee06630fd6f904788d2f0ab005cce45aadb8fb73889\
scikit-surprise==1.1.3\
typing_extensions==4.8.0\
And some other libraries that come built-in.


## Information
+ The datatset and the intermediary data files are saved in the directory named 'data'.
+ The plots are saved in the directory named 'plots'.
+ The recommendation dataset should be placed in the same directory with the code. The dataset consists of the following files: train.tsv, test.tsv, metadata.tsv.
+ The trained word embedding model (GoogleNews-vectors-negative300.bin) was downloaded from here: https://code.google.com/archive/p/word2vec/.
+ The project consists of both .ipynb and .py files, that need to be placed in the same location. The .py files contain utility function and classes the can be used across the whole project.
+ constants.py contains the contants that can be used across the whole project.