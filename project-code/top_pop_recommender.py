from __future__ import annotations
from surprise.prediction_algorithms.predictions import Prediction
from typing import List
import pandas as pd


class TopPopRecommender:
    def __init__(self) -> None:
        self.dataset = pd.DataFrame()
        self.item_frequency = pd.Series(dtype='int32')

    def fit(self, dataset: pd.DataFrame) -> TopPopRecommender:
        self.dataset = dataset
        self.item_frequency = dataset['item_id'].value_counts().sort_values(ascending=False)

        return self

    def recommend(self, user_id: int, k: int, exclude_downloaded: bool = True) \
            -> List[Prediction]:
        top_k_recs = list()
        if exclude_downloaded:
            downloaded_items = self.dataset[self.dataset['user_id'] == user_id]['item_id']
            top_items = self.item_frequency.drop(downloaded_items)[:k]
        else:
            top_items = self.item_frequency[:k]

        for i, item_id in enumerate(top_items.index):
            top_k_recs.append(Prediction(user_id, item_id, 1/(i + 1), 1/(i + 1), dict()))

        return top_k_recs
