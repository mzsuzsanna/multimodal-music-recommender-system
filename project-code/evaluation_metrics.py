from collections import defaultdict
from typing import Dict, List
import numpy as np
import pandas as pd
from surprise.prediction_algorithms.predictions import Prediction


def get_top_k_user_i(predictions: Dict[str, List],
                     user_id: str,
                     k: int) -> List:
    """Retrieve the top-K recommended items for a given user based on
    the content information.
    Args:
        predictions: A dictionary containing the similarities
            between users and items (e.g., keys are user ids,
            values are dictionaries containing the similarity
            of the corresponding user to each item)
        user_id(str): The user we want to extract top-K recommendations for
        k(int): The number of recommendation to output for each user.
    Returns:
        A dict where keys are user ids and values are lists of tuples:
        [(item id, similarity), ...] of size n.    """

    top_k = sorted(predictions[user_id], key=lambda x: x[1], reverse=True)[:k]
    return top_k


def get_top_k(predictions: List[Prediction],
              k: int) -> Dict[str, List]:
    """Compute the top-K recommendation for each user from a set of predictions.
    Args:
        predictions(list of Prediction objects): The list of predictions, as
            returned by the test method of an algorithm.
        k(int): The number of recommendation to output for each user.
    Returns:
        A dict where keys are user (raw) ids and values are lists of tuples:
        [(raw item id, rating estimation), ...] of size n.
    """
    topk = defaultdict(list)

    for uid, iid, _, est, _ in predictions:
        topk[uid].append((iid, est))

    for uid, ratings in topk.items():
        ratings.sort(key=lambda x: x[1], reverse=True)
        topk[uid] = ratings[:k]

    return topk


def print_top_k(user_id: str, topk: Dict[str, List]) -> None:
    user_ratings = topk[user_id]
    print(f"TOP-{len(user_ratings)} predictions for user {user_id}: "
          f"{[(item, round(rating, 2)) for (item, rating) in user_ratings]}")


def precision_at_k(top_k_recs: Dict[str, List],
                   df_test: pd.DataFrame,
                   k: int) -> Dict[str, float]:
    """Compute precision at k for each user.
    Args:
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        df_test: Pandas DataFrame containing user-item ratings in 
            the test split.
        k(int): The number of recommendation to output for each user.
    Returns:
        A dict where keys are user ids (str)
        and values are the P@k (float) for each of them
    """

    precisions = defaultdict(float)

    for user_id, ratings in top_k_recs.items():
        rel_ratings = []
        for item_id, _ in ratings:
            if df_test[(df_test['user_id'] == user_id) & (df_test['item_id'] == item_id)].empty:
                rel_ratings.append(0)
            else:
                rel_ratings.append(1)

        precisions[user_id] = sum(rel_ratings) / k

    return precisions


def averaged_precision_at_k(top_k_recs: Dict[str, List],
                            df_test: pd.DataFrame,
                            k: int) -> float:
    """Compute precision at k averaged over all users.
    Args:
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        df_test: Pandas DataFrame containing user-item ratings in
            the test split.
        k(int): The number of recommendation to output for each user.
    Returns:
        P@k (float) averaged over all users
    """

    precisions_nb = precision_at_k(top_k_recs, df_test, k=k)
    avg_pk = sum(prec for prec in precisions_nb.values()) / len(precisions_nb)

    return avg_pk


def mean_average_precision(top_k_recs: Dict[str, List],
                           df_test: pd.DataFrame) -> float:
    """Compute the mean average precision.
    Args:
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        df_test: Pandas DataFrame containing user-item ratings in 
            the test split.
    Returns:
        The MAP@k (float)
    """

    average_precision_users = []

    for user_id, ratings in top_k_recs.items():
        ap = 0
        nr_rel_items = 0
        rel_items_for_user = df_test[(df_test['user_id'] == user_id)]
        for i, (item_id, _) in enumerate(ratings):
            if not rel_items_for_user[rel_items_for_user['item_id'] == item_id].empty:
                nr_rel_items += 1
                ap += nr_rel_items / (i + 1)

        if nr_rel_items > 0:
            ap /= len(rel_items_for_user)
        average_precision_users.append(ap)

    mapk = np.mean(average_precision_users)
    return mapk


def mean_reciprocal_rank(top_k_recs: Dict[str, List],
                         df_test: pd.DataFrame) -> float:
    """Compute the mean reciprocal rank.
    Args:
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        df_test: Pandas DataFrame containing user-item ratings in 
            the test split.
    Returns:
        The MRR@k (float)
    """

    reciprocal_rank = []

    for user_id, ratings in top_k_recs.items():
        rr = 0
        for i, (item_id, _) in enumerate(ratings):
            if not df_test[(df_test['user_id'] == user_id) &
                           (df_test['item_id'] == item_id)].empty:
                rr = 1 / (i + 1)
                break
        reciprocal_rank.append(rr)

    mean_rr = np.mean(reciprocal_rank)
    return mean_rr


def hit_rate(top_k_recs: Dict[str, List],
             df_test: pd.DataFrame) -> float:
    """Compute the hit rate.
    Args:
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        df_test: Pandas DataFrame containing user-item ratings in
            the test split.
    Returns:
        The average hit rate
    """
    hits = 0

    for user_id, ratings in top_k_recs.items():
        for item_id, _ in ratings:
            if not df_test[(df_test['user_id'] == user_id) & (df_test['item_id'] == item_id)].empty:
                hits += 1
                break

    hits /= len(top_k_recs)
    return hits


def coverage(total_nr_items: int,
             top_k_recs: Dict[str, List]) -> float:
    """Compute the coverage.
    Args:
        total_nr_items (int): total number of items in the dataset
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
    Returns:
        The coverage (float)
        total_nr_items:
    """
    recommended_items = set([item[0] for rec in top_k_recs.values() for item in rec])
    catalogue_cov = len(recommended_items) / total_nr_items
    return catalogue_cov


def calculate_metrics(total_nr_items: int,
                      top_k_recs: Dict[str, List],
                      df_test: pd.DataFrame,
                      k: int) -> None:
    """Compute all utility based test metrics.
    Args:
        total_nr_items (int): total number of items in the dataset
        top_k_recs: A dict where keys are user ids and values are lists of tuples:
            [(item id, rating estimation/similarity), ...] of size n.
        k(int): The number of recommendation to output for each user.
        df_test: Pandas DataFrame containing user-item ratings in
        the test split.
    """
    # Averaged precision
    prec_result = averaged_precision_at_k(top_k_recs, df_test, k)
    print(f'Averaged P@{k}: {prec_result:.3g}')
    # MAP
    map_result = mean_average_precision(top_k_recs, df_test)
    print(f'MAP@{k}: {map_result:.3g}')
    # MRR
    mrr_result = mean_reciprocal_rank(top_k_recs, df_test)
    print(f'MRR@{k}: {mrr_result:.3g}')
    # Hit rate
    hr_result = hit_rate(top_k_recs, df_test)
    print(f'Hit Rate (top-{k}): {hr_result:.3g}')
    # Coverage
    coverage_result = coverage(total_nr_items, top_k_recs)
    print(f'Coverage: {coverage_result:.3g}')


def calculate_metrics_content_based(test_df: pd.DataFrame, user_item_sims: Dict[str, List]) -> None:
    """Compute and display all the metrics for the users in the test split,
    using the calculated similarities of a content-based system.
    Args:
        test_df: Pandas DataFrame containing user-item ratings in
            the test split.
        user_item_sims: calculated similarities between users and items in the list
            of unobserved ratings.
    """
    top_5 = {}
    test_user_ids = list(set(test_df['user_id']))
    for user_id in test_user_ids:
        top_5[user_id] = get_top_k_user_i(user_item_sims, user_id, 5)

    calculate_metrics(len(set(test_df['user_id'])), top_5, test_df, 5)
