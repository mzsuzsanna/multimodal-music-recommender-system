# file path and name constants
DATA_PATH = '..\\data\\'
PLOTS_PATH = '..\\plots\\'
AUDIO_PATH = f'{DATA_PATH}audios\\'
AUDIO_LENGTH = 90
REL_NER_FILE_NAME = f'{DATA_PATH}relevant_entities.pickle'
COMMON_WORDS_FILE_NAME = f'{DATA_PATH}most_common_words.pickle'
UNIQUE_WORDS_FILE_NAME = f'{DATA_PATH}unique_words.pickle'
UNIQUE_TAGS_FILE_NAME = f'{DATA_PATH}unique_tags.pickle'
CLEANED_META_DATA_FILE_NAME = f'{DATA_PATH}cleaned_meta_data.pickle'
META_DATA_FILE_WITH_AUDIO_NAME = f'{DATA_PATH}cleaned_meta_data_with_audio.pickle'
COMMON_TAGS_FILE_NAME = f'{DATA_PATH}most_common_tags.pickle'
YOUTUBE_SCRIPT_PATH = '..\\ytdownload_scripts\\'

# 91 tags to be removed
TAGS_TO_BE_REMOVED = {'favorites', 'awesome', 'love-at-first-listen', 'favorite', 'favourite', 'favourites', 
                      'favorite-songs', 'loved', 'love-it', 'great', 'favourite-songs', 'personal-favourites', 
                      'great-song', 'like', 'best', 'good', 'best-song-ever', 'my-favorites', 'good-music', 
                      'i-love-it', 'wow', 'my-music', 'favs', 'favorite-tracks', 'my-favorite', 'songs-i-love', 'like-it', 
                      'favorite-band', 'favies', 'favorite-songs-ever', 'on-my-ipod', 'describes-me',
                      'to-listen', 'wish-list', 'amazing-songs', 'fav-tracks', 'i-love-these-songs', 'my-favorite-artists', 
                      'fav1', 'my-faves', 'favorite-artist', 'favorit', 'favorita', 'favourite-tunes', 'favorite-singers',
                      'my-fav', 'fav-songs', 'm-favs', 'fave-tracks', 'fave-songs', 'myfavo', 'personal-favourite',
                      'fav-song', 'favourite-song', 'myfav', 'my-other-fav', 'new-favorite', 'mis-favoritas', 'fav-tag',
                      'new-favs', 'fav-artists', 'my-fav-bands', 'absolutte-favoritter', 'my-favorite-albums',
                      'absolute-favorite', 'favourite-songs-ever', 'some-of-my-fav-stuff', 'new-favorites', 'favori',
                      'current-favorite-songs', 'favorite-track', 'one-of-my-favorites', 'favorite-music',
                      'favoritesss', 'current-favourites', 'personal-favorites', 'one-of-my-favorite-songs',
                      'mynewmusicpicksoftheweek', 'my-songs', 'my-soundtrack', 'my-song', 'mytoptracks', 
                      'songs-of-my-life', 'my-essential-songs', 'this-is-my-song', 'my-loved-tracks',
                      'my-top-musics', 'my-number-one', 'my-best', 'my-radio', 'mypreferred', 'some-of-my-fav-stuff'}
# model parameter constants
REL_NER_TYPES = ['PERSON', 'NORP', 'FAC', 'ORG', 'GPE', 'WORK_OF_ART', 'LOC', 'EVENT']
NEGATIVE_SAMPLE_RATIOS = [1, 2, 5, 10]
NEG_SAMPLING_METHODS = ['uniform', 'popularity']
RANDOM_SEED = 0
NR_FOLDS = 5
RANK_K = 5
PARAMS_KNN = {
    'k': [3, 5, 10, 20, 30],
    'sim_options': [{'name': 'msd'},
                   {'name': 'pearson'}]
}
PARAMS_SVD = {
    'n_epochs': [50, 100, 150],
    'n_factors': [2, 3, 4, 5, 10, 20, 30]
}
ALPHA = 0.2
USER_ACTIVITY_THRESH = 50
CUSTOM_SEARCH_QUERY_LENGTH = 100