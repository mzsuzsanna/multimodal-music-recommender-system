import string
from collections import Counter
from typing import List

import en_core_web_sm
import pandas as pd
import numpy as np
import pickle
# nltk.download('punkt')
from spacy.lang.en import stop_words
from nltk.stem import SnowballStemmer
from nltk.tokenize import MWETokenizer
from nltk.tokenize import word_tokenize
from tqdm import tqdm

from constants import (REL_NER_TYPES, REL_NER_FILE_NAME, COMMON_WORDS_FILE_NAME,
                       UNIQUE_WORDS_FILE_NAME, UNIQUE_TAGS_FILE_NAME)


def dummy_tokenizer(text: List[str]) -> List[str]:
    return text


def tokenize_tags(text: str):
    """Normalization of the input tags: lowercasing, tokenization, stop words,
       most common words, and removing low-frequency tags.
        Args:
            text: input text.
        Returns:
            Return the normalized list of tokens.
    """
    with open(UNIQUE_TAGS_FILE_NAME, 'rb') as f:
        unique_words = pickle.load(f)

    with open(COMMON_WORDS_FILE_NAME, 'rb') as f:
        most_common = pickle.load(f)

    tokenized_text = word_tokenize(text.lower())
    tokenized_text = [word for word in tokenized_text if (word not in stop_words.STOP_WORDS) and
                      (word not in TAGS_TO_BE_REMOVED) and (word not in most_common)]
    tokenized_text = [word for word in tokenized_text if word not in unique_words]
    return tokenized_text


def save_relevant_ners(df_column: pd.Series) -> None:
    """Save relevant named entities to file. 
    Args:
        df_column: column of a dataframe that contains textual data.  
    """
    # find relevant entities
    nlp = en_core_web_sm.load()
    preproc_pipe = []
    for doc in tqdm(nlp.pipe(df_column, batch_size=20)):
        preproc_pipe.extend([X.text.lower() for X in doc.ents if X.label_ in REL_NER_TYPES])
    rel_entities = []
    for ent in preproc_pipe:
        splitted = tuple([word.lower() for word in ent.replace('\'s', '').split() if (word not in string.punctuation)])
        rel_entities.append(splitted)

    # Save results for future use
    with open(REL_NER_FILE_NAME, 'wb') as f:
        pickle.dump(rel_entities, f)


def save_most_common_words(df_column: pd.Series) -> None:
    """Save top-100 common words from a dataframe column to file. 
    Args:
        df_column: column of a dataframe that contains textual data.  
    """
    new_df = df_column.apply(normalize_text_simple)
    stemmer = SnowballStemmer(language='english')
    stemmed_text = [stemmer.stem(word) for word in flatten_list(new_df.values)]
    most_common = [word for word, _ in Counter(stemmed_text).most_common(100)]

    # Save results for future use
    with open(COMMON_WORDS_FILE_NAME, 'wb') as f:
        print(most_common)
        pickle.dump(most_common, f)


def save_unique_words(df_column: pd.Series) -> None:
    """Save unique words (whose frequency is 1) from a dataframe column to file.
    Args:
        df_column: column of a dataframe that contains textual data.
    """
    new_df = df_column.apply(normalize_text_simple)
    stemmer = SnowballStemmer(language='english')
    stemmed_text = [stemmer.stem(word) for word in flatten_list(new_df.values)]
    token_freq = Counter(stemmed_text)
    unique_words = [word for word in token_freq if token_freq[word] == 1]

    # Save results for future use
    with open(UNIQUE_WORDS_FILE_NAME, 'wb') as f:
        pickle.dump(unique_words, f)


def save_unique_tags(df_column: pd.Series) -> None:
    """Save unique tags (whose frequency is 1) from a dataframe column to file.
    Args:
        df_column: column of a dataframe that contains textual data.
    """
    new_df = df_column.apply(normalize_text_simple)
    token_freq = Counter(flatten_list(new_df.values))
    unique_words = [word for word in token_freq if token_freq[word] == 1]

    # Save results for future use
    with open(UNIQUE_TAGS_FILE_NAME, 'wb') as f:
        pickle.dump(unique_words, f)


def normalize_text_simple(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, tokenization, 
    removing punctuation, stop words and digits. 
    Args:
        text: input text.  
    Returns:
        Return the normalized list of tokens.
    """

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = word_tokenize(text.lower())
    tokenized_text = [word for word in tokenized_text if (word not in string.punctuation) and
                      (word not in stop_words.STOP_WORDS) and (not word.isdigit())]
    return tokenized_text


def normalize_text_custom_stopw(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, tokenization, 
    removing punctuation, stop words (including custom list of stop words) and digits. 
    Args:
        text: input text.  
    Returns:
        Return the normalized list of tokens.
    """
    with open("most_common_words.pickle", 'rb') as f:
        most_common = pickle.load(f)

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = normalize_text_simple(text)
    stemmer = SnowballStemmer(language='english')
    tokenized_text = [word for word in tokenized_text if
                      (word in ['love', 'nme', 'american', 'girl', 'come', 'mtv', 'rock', 'relationship',
                                'night', 'went', 'go', 'feel', 'friend', 'got', 'thought', 'differ', 'world',
                                'life', 'time']) or
                      (stemmer.stem(word) not in most_common)]
    return tokenized_text


def normalize_text_custom_stopw_stem(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, tokenization, stemming,
    removing punctuation, stop words (including custom list of stop words) and digits.
    Args:
        text: input text.
    Returns:
        Return the normalized list of tokens.
    """

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = normalize_text_custom_stopw(text)
    stemmer = SnowballStemmer(language='english')
    tokenized_text = [stemmer.stem(word) for word in tokenized_text]
    return tokenized_text


def normalize_text_custom_stopw_stem_deluniq(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, tokenization, stemming,
    removing punctuation, stop words (including custom list of stop words) and digits,
    deleting unique words.
    Args:
        text: input text.
    Returns:
        Return the normalized list of tokens.
    """
    with open("unique_words.pickle", 'rb') as f:
        unique_words = pickle.load(f)

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = normalize_text_custom_stopw(text)
    stemmer = SnowballStemmer(language='english')
    tokenized_text = [stemmer.stem(word) for word in tokenized_text]
    tokenized_text = [word for word in tokenized_text if word not in unique_words]
    return tokenized_text


def normalize_text_multiw(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, expression tokenization,
   removing punctuation, stop words and digits.
   Args:
       text: input text.
   Returns:
       Return the normalized list of tokens.
   """
    with open(REL_NER_FILE_NAME, 'rb') as f:
        rel_entities = pickle.load(f)

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenizer = MWETokenizer(rel_entities)
    tokenized_text = tokenizer.tokenize(text.split())
    stemmed_text = [word.lower() for word in tokenized_text if (word not in string.punctuation) and
                    (word.lower() not in stop_words.STOP_WORDS) and (not word.isdigit())]
    tokens = []
    for i, token in enumerate(stemmed_text):
        if '_' in token:
            tokens.append(token.replace('_', ' '))
        else:
            tokens.append(token)
    return tokens


def normalize_text_multiw_stem(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, stemming,
    expression tokenization, removing punctuation, stop words and digits.
    Args:
       text: input text.
    Returns:
       Return the normalized list of tokens.
    """

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = normalize_text_multiw(text)
    stemmer = SnowballStemmer(language='english')
    tokenized_text = [stemmer.stem(word) for word in tokenized_text]
    return tokenized_text


def normalize_text_multiw_custom_stopw(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, expression tokenization, 
    removing punctuation, stop words (including custom list of stop words) and digits. 
    Args:
        text: input text.  
    Returns:
        Return the normalized list of tokens.
    """
    with open(COMMON_WORDS_FILE_NAME, 'rb') as f:
        most_common = pickle.load(f)

    with open(REL_NER_FILE_NAME, 'rb') as f:
        rel_entities = pickle.load(f)

    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenizer = MWETokenizer(rel_entities)
    tokenized_text = tokenizer.tokenize(text.split())
    stemmer = SnowballStemmer(language='english')
    stemmed_text = [word.lower() for word in tokenized_text if (word not in string.punctuation) and
                    (word.lower() not in stop_words.STOP_WORDS) and (not word.isdigit())]
    tokens = []
    for i, token in enumerate(stemmed_text):
        if '_' in token:
            tokens.append(token.replace('_', ' '))
        else:
            tokens.append(token)
    tokens = [word for word in tokens if
              (word in ['love', 'nme', 'american', 'girl', 'come', 'mtv', 'rock', 'relationship',
                        'night', 'went', 'go', 'feel', 'friend', 'got', 'thought', 'differ', 'world',
                        'life', 'time']) or
              (stemmer.stem(word) not in most_common)]
    return tokens


def normalize_text_multiw_custom_stopw_stem(text: str) -> List[str]:
    """Normalization of the input text: lowercasing, stemming, expression tokenization,
    removing punctuation, stop words (including custom list of stop words) and digits.
    Args:
        text: input text.
    Returns:
        Return the normalized list of tokens.
    """
    text = text.replace(" 's", "").replace("``", "").replace("''", "")
    tokenized_text = normalize_text_multiw_custom_stopw(text)
    stemmer = SnowballStemmer(language='english')
    tokenized_text = [stemmer.stem(word) for word in tokenized_text]
    return tokenized_text


def flatten_list(nested_list: np.ndarray[List[str]]) -> List[str]:
    """Flatten nested lists.
   Args:
       nested_list: a list of lists.
   Returns:
       Flattened list.
   """
    return [l for ls in nested_list for l in ls]
