import pandas as pd
import numpy as np
from tqdm import tqdm
from typing import Dict, List, Tuple
from collections import defaultdict
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def get_user_profiles_tfidf(train_df: pd.DataFrame, tfidf_vec: TfidfVectorizer) -> Dict[str, List]:
    """Compute the user profiles based on the items the users downloaded.
    Args:
        train_df: Pandas DataFrame containing user-item ratings in
            the train split.
        tfidf_vec: instance of TfidfVectorizer class containing all the calculated tf-idf scores.
    Returns:
        User profiles.
    """
    user_ids = set(train_df['user_id'])
    item_ids = list(set(train_df['item_id'].values))
    user_profiles = dict()
    for user_id in user_ids:
        user_ratings = train_df[train_df['user_id'] == user_id]
        mapped_item_ids = [item_ids.index(item_id) for item_id in user_ratings['item_id'].values]
        user_profiles[user_id] = np.sum(list(tfidf_vec[mapped_item_ids]), axis=0) / len(mapped_item_ids)
    return user_profiles


def calc_similarities_tfidf(train_df: pd.DataFrame, tfidf_vec: TfidfVectorizer,
                            unobserved_ratings: List[Tuple[int, str, float]]) -> Dict[str, List]:
    """Calculate similarities between users and items in the list of unobserved ratings.
    Args:
        train_df: Pandas DataFrame containing user-item ratings in
            the train split.
        tfidf_vec: instance of TfidfVectorizer class containing all the calculated tf-idf scores.
        unobserved_ratings: (user, item, rating) tuples for all the (user, item) pairs that
            are not in the train split.
    Returns:
        Similarities between users and items in the list of unobserved ratings.
    """
    item_ids = list(set(train_df['item_id'].values))
    user_profiles: Dict[str, List] = get_user_profiles_tfidf(train_df, tfidf_vec)

    user_item_sims = defaultdict(lambda: list())
    for user_id, item_id, _ in tqdm(unobserved_ratings):
        user_item_sims[user_id].append((item_id, cosine_similarity(user_profiles[user_id].reshape(-1, 1),
                                                                   tfidf_vec[item_ids.index(item_id)].reshape(-1, 1))[
            0][0]))
    return user_item_sims


def calc_similarities_for_item_dict(train_df: pd.DataFrame, item_profiles: Dict[str, np.array],
                                    unobserved_ratings: List[Tuple[int, str, float]]) -> Dict[str, List]:
    """Calculate similarities between users and items in the list of unobserved ratings.
    Args:
        train_df: Pandas DataFrame containing user-item ratings in
            the train split.
        item_profiles: dictionary of items represented numerically as embeddings.
        unobserved_ratings: (user, item, rating) tuples for all the (user, item) pairs that
            are not in the train split.
    Returns:
        Similarities between users and items in the list of unobserved ratings
    """
    # build user profiles
    user_ids = set(train_df['user_id'])
    user_profiles = dict()
    for user_id in user_ids:
        user_ratings = train_df[train_df['user_id'] == user_id]
        user_embed = []
        for item_id in user_ratings['item_id'].values:
            user_embed.append(item_profiles[item_id])
        user_profiles[user_id] = np.sum(user_embed, axis=0) / len(user_embed)

    # Cosine similarities
    user_item_sims = defaultdict(lambda: list())
    for user_id, item_id, _ in tqdm(unobserved_ratings):
        user_item_sims[user_id].append((item_id, cosine_similarity(user_profiles[user_id].reshape(1, -1),
                                                                   item_profiles[item_id].reshape(1, -1))[0][0]))
    return user_item_sims


def compute_item_profiles_from_word_embeds(word_embeds, processed_meta_data: pd.Series) -> Dict[str, np.array]:
    """Calculates item profiles computed as the average of all the word embeddings present in the description.
    Args:
        word_embeds: word embeddings.
        processed_meta_data: preprocessed data column from the dataframe
    Returns:
        Returns the item profiles represented as embeddings.
    """
    item_profiles = dict()
    # loop through all the items
    for idx in processed_meta_data.index:
        desc = processed_meta_data.loc[idx]
        stack_word_embeds = []
        for token in desc:
            try:
                stack_word_embeds.append(word_embeds[token])
            # if token does not have an embedding, it is left out
            except KeyError:
                pass
        item_profiles[idx + 1] = np.average(stack_word_embeds, axis=0)

    return item_profiles
